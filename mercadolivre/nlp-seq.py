'''
Created on 15 de set de 2019

@author: savio
'''

import nltk
import pandas as pd
from nltk.tokenize import word_tokenize
from sklearn import naive_bayes, svm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing.label import LabelEncoder

if __name__ == '__main__':
    
    corpus = pd.read_csv('../data/train.csv', encoding='utf-8')

    # Step - a : Remove blank rows if any.
    corpus['title'].dropna(inplace=True)
    # Step - b : Change all the text to lower case.
    corpus['title'] = [entry.lower() for entry in corpus['title']]
    # Step - c : Tokenization : In this each entry in the corpus will be broken into set of words
    corpus['title'] = [word_tokenize(entry) for entry in corpus['title']]

    stop_words = nltk.corpus.stopwords.words('portuguese') + [',', '|', ';', '+'
        , 'claro', 'escuro', 'amarelo', 'azul', 'bege', 'branco', 'bronze', 'castanho', 'cinza', 'dourado', 'laranja',
                                                              'marrom', 'prata', 'preto', 'rosa', 'roxo', 'verde',
                                                              'vermelho', 'violeta'
                                                              ]
    for index, entry in enumerate(corpus['title']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word in entry:
            if word not in stop_words and word.isalpha():
                Final_words.append(word)
        # The final processed set of words for each iteration will be stored in 'text_final'
        corpus.loc[index, 'text_final'] = str(Final_words)


    X_train, X_test, y_train, y_test = train_test_split(corpus[['text_final']], corpus.category, test_size=0.2, random_state=42)
    Encoder = LabelEncoder()
    y_train = Encoder.fit_transform(y_train)
    y_test = Encoder.fit_transform(y_test)

    Tfidf_vect = TfidfVectorizer()
    Train_X_Tfidf = Tfidf_vect.fit_transform(X_train).toarray()
    Test_X_Tfidf = Tfidf_vect.transform(X_test).toarray()

    print(Tfidf_vect.vocabulary_)

    # fit the training dataset on the NB classifier
    Naive = naive_bayes.MultinomialNB()
    naive_fit = Naive.fit(Train_X_Tfidf, y_train)
    score = naive_fit.score(Test_X_Tfidf, y_test)
    print("Naive Bayes Score: ", score)

    # fit the training dataset on the SVM classifier
    SVM = svm.SVC(C=1.0, kernel='linear', degree=3, gamma='auto')
    svm_fit = SVM.fit(Train_X_Tfidf, y_train)
    score = svm_fit.score(Test_X_Tfidf, y_test)
    print("SVM Score: ", score)

    # fit the training dataset on the SVM classifier
    LR = LogisticRegression(n_jobs=-1, multi_class='multinomial', solver='lbfgs')
    lr_fit = LR.fit(Train_X_Tfidf, y_train)
    score = lr_fit.score(Test_X_Tfidf, y_test)
    print("SVM Score: ", score)

    pass
