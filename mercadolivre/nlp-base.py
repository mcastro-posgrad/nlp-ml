'''
Created on 15 de set de 2019

@author: marcelo
'''

import time
import nltk
import unicodedata
import sys
import gc

from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import mac_morpho

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.ensemble.voting import VotingClassifier

from sklearn.model_selection import train_test_split
from sklearn.model_selection._search import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import model_selection, naive_bayes, svm
import string
from sklearn import metrics
from xgboost import XGBClassifier

def sentence_token(sentence:str, lang:str='english') -> ([str]):
    return sent_tokenize(sentence, language=lang)


def word_token(sentence:str, lang:str='english') -> ([str]):
    return word_tokenize(sentence, language=lang)


def stemization(tokens:[str], lang:str='english') -> ([str]):
    return [SnowballStemmer(lang).stem(item) for item in tokens]


def rmdiacriticword(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')


def rmdiacritics(tokens:[str]):
    return [rmdiacriticword(item) for item in tokens]

# portuguese nlp


def tokenization(document):
    document = ''.join(c for c in document if not c.isdigit())
    document = ''.join(c for c in document if c not in string.punctuation)
    tokens = word_token(document, 'portuguese')
    return tokens

def preprocess(document):
    document = ''.join(c for c in document if not c.isdigit())
    document = ''.join(c for c in document if c not in string.punctuation)
    return document

class StemmedTfidfVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer, self).build_analyzer()
        return lambda doc: stemization(rmdiacritics(analyzer(doc)), lang='spanish')

def sentence_features(st, ix):
    d_ft = {}
    d_ft['word'] = st[ix]
    d_ft['dist_from_first'] = ix - 0
    d_ft['dist_from_last'] = len(st) - ix
    d_ft['capitalized'] = st[ix][0].upper() == st[ix][0]
    d_ft['prefix1'] = st[ix][0]
    d_ft['prefix2'] = st[ix][:2]
    d_ft['prefix3'] = st[ix][:3]
    d_ft['suffix1'] = st[ix][-1]
    d_ft['suffix2'] = st[ix][-2:]
    d_ft['suffix3'] = st[ix][-3:]
    d_ft['prev_word'] = '' if ix == 0 else st[ix - 1]
    d_ft['next_word'] = '' if ix == (len(st) - 1) else st[ix + 1]
    d_ft['numeric'] = st[ix].isdigit()
    return d_ft


def get_untagged_sentence(tagged_sentence):
    [s, t] = zip(*tagged_sentence)
    return list(s)


def ext_ft(tg_sent):
    sent, tag = [], []
 
    for tg in tg_sent:
        for index in range(len(tg)):
            try:
                sent.append(sentence_features(get_untagged_sentence(tg), index))
                tag.append(tg[index][1])
            except:
                pass    
 
    return sent, tag

def create_transformation(v):
    tagged_sentences = nltk.corpus.mac_morpho.tagged_sents()
    
    X, y = ext_ft(tagged_sentences)
    
    # transform only sample
    n_sample = 20000
    X = X[0:n_sample]
    y = y[0:n_sample]
    
    X_transformed = v.fit_transform(X)
    
    dfx = pd.DataFrame(X_transformed)
    dfy = pd.DataFrame(y)
    
    return dfx, dfy

# Transforma string em vetor e usa o modelo treinado
def predict_pos_tags(v, clf, sentence):
    tagged_sentence = []
    features = [sentence_features(sentence, index) for index in range(len(sentence))]
    features = v.transform(features)
    tags     = clf.predict(features)
    return zip(sentence, tags)

def create_postaggin_features():
    
    v = DictVectorizer(sparse=False)
    dfx, dfy = create_transformation(v)
    
    X_train,X_test,y_train,y_test = train_test_split(dfx, dfy, test_size=0.2, random_state=123)

    dfx = dfy = None
    gc.collect()
    
    rf = LogisticRegression()
    clf = rf.fit(X_train,y_train)
    
    score = clf.score(X_test, y_test)
    print("Score: ", score)

    # sentence
    test_sentence = "O rato roeu a roupa do rei de Roma"
    
    # predict
    for tagged in predict_pos_tags(v, clf, test_sentence.split()):
        print(tagged)
    
def clearSeparadores(corpus):
    corpus['title'] = corpus.title.str.replace(',', ' ')
    corpus['title'] = corpus.title.str.replace('|', ' ')
    corpus['title'] = corpus.title.str.replace(';', ' ')
    corpus['title'] = corpus.title.str.replace('-', ' ')
    corpus['title'] = corpus.title.str.replace('+', ' ')
    corpus['title'] = corpus.title.str.replace('&', ' ')
    corpus['title'] = corpus.title.str.replace('/', ' ')
    corpus['title'] = corpus.title.str.replace(')', ' ')
    corpus['title'] = corpus.title.str.replace('(', ' ')
    corpus['title'] = corpus.title.str.replace('%', ' ')

if __name__ == '__main__':
    
#     create_postaggin_features()
    
#     corpus = pd.read_csv('../articles.csv')
    corpus = pd.read_csv('../data/train.csv', encoding='utf-8')
    
    # Comment to get total corpus 
#     n_sample = 10000
#     corpus = corpus[0:n_sample]

    clearSeparadores(corpus)
    
    X_train, X_test, y_train, y_test = train_test_split(corpus[['title']], corpus.category, test_size=0.2, random_state=43)

    stop_words = nltk.corpus.stopwords.words('portuguese')
#     stop_words = []

    clf1 = LogisticRegression()
    clf2 = MultinomialNB()
    clf3 = svm.SVC(C=1.0, kernel='linear', gamma='auto', decision_function_shape='ovo', probability=True)
    clf4 = RandomForestClassifier(n_estimators=50)
    clf5 = KNeighborsClassifier(n_neighbors=7)
    clf6 = XGBClassifier()
    eclf1 = VotingClassifier(estimators=[('lr', clf1), ('nb', clf2), ('svm', clf3)], voting='soft')
    eclf2 = VotingClassifier(estimators=[('lr', clf1), ('nb', clf2), ('svm', clf3), ('rf', clf4), ('nn', clf5), ('xg', clf6)], voting='soft')

    # NLP Pipeline
    text_clf = Pipeline([
                    # Vectorize
                    ('vect', StemmedTfidfVectorizer(
                                            # preprocessor=preprocess,
                                            tokenizer=tokenization,
                                            stop_words=stop_words,
                                            encoding='utf-8',
                                            sublinear_tf=True,
                                            smooth_idf=False,
                                            ngram_range=(1, 2))),
                    # Classificador
                    ('clf', OneVsRestClassifier(clf3)),
                ])

    parameters = {'clf__C': np.linspace(0, 1.0, 5), 'clf__kernel': ('linear', 'rbf'), 'clf__decision_function_shape': ('ovo', 'ovr')}

    # text_clf = GridSearchCV(text_clf, parameters, cv=3, iid=False, n_jobs=-1)

    # Train
    text_clf = text_clf.fit(X_train.title, y_train)

    # Evaluate Grid Search
    # print("Best Score: ", text_clf.best_score_)
    # for param_name in sorted(parameters.keys()):
    #     print("%s: %r" % (param_name, text_clf.best_params_[param_name]))

    # Evaluate
    score = text_clf.score(X_test.title, y_test)
    print("Score: ", score)

    prediction = True
    detailed_performance = True
    confusion = False
    
    if prediction:
        testbase = pd.read_csv('../data/test.csv', encoding='utf-8')
        clearSeparadores(testbase)
        predictions = text_clf.predict(testbase.title)
        result = pd.DataFrame({'category': predictions})
        result.index.name = 'id'
        result.to_csv('../data/result.csv')

    if detailed_performance:
        predictions = text_clf.predict(X_test.title)
        print(metrics.classification_report(y_test, predictions))

    if confusion:
        predictions = text_clf.predict(X_test.title)
        conf_matrix = confusion_matrix(y_test, predictions)
        
        testdf = pd.DataFrame();
        testdf['title'] = X_test.title
        testdf['category'] = y_test
        testdf['prediction'] = predictions
        testdf['check'] = testdf.category == testdf.prediction
        testdf.to_csv('../data/testpred.csv')
        
        plt.figure(figsize=(10, 10))
        plt.xticks(np.arange(len(text_clf.classes_)), text_clf.classes_)
        plt.yticks(np.arange(len(text_clf.classes_)), text_clf.classes_)
        plt.imshow(conf_matrix, cmap=plt.cm.Blues)
        plt.colorbar()
        plt.show()
    
    pass
